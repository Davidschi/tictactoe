class TicTacToe():
    option = [" ", "X", "O"]

    def __init__(self):
        self.board = {
            1: self.option[0],
            2: self.option[0],
            3: self.option[0],
            4: self.option[0],
            5: self.option[0],
            6: self.option[0],
            7: self.option[0],
            8: self.option[0],
            9: self.option[0]
        }

    def print_board(self):
        print(f"""
  {self.board[1]}|{self.board[2]}|{self.board[3]}
  {self.board[4]}|{self.board[5]}|{self.board[6]}
  {self.board[7]}|{self.board[8]}|{self.board[9]}
""")

    def place(self, player, location):
        self.board[location] = player

    def win(self, playernumber):
        player = self.option[playernumber]
        if self.board[1] is player and self.board[2] is player and self.board[3] is player or \
           self.board[4] is player and self.board[5] is player and self.board[6] is player or \
           self.board[7] is player and self.board[8] is player and self.board[9] is player or \
           self.board[1] is player and self.board[4] is player and self.board[7] is player or \
           self.board[2] is player and self.board[5] is player and self.board[8] is player or \
           self.board[3] is player and self.board[6] is player and self.board[9] is player or \
           self.board[1] is player and self.board[5] is player and self.board[9] is player or \
           self.board[7] is player and self.board[5] is player and self.board[3] is player:
            print(f"Player {playernumber} wins!")
            return True
        if self.board[1] != self.option[0] and self.board[2] != self.option[0] and self.board[3] != self.option[0] and \
           self.board[4] != self.option[0] and self.board[5] != self.option[0] and self.board[6] != self.option[0] and \
           self.board[7] != self.option[0] and self.board[8] != self.option[0] and self.board[9] != self.option[0]:
            print("It's a tie!")
            return "tie"
        return False


def game(instance):

    def invalid_input():
        print("That's not a valid input.")
        instance.print_board()

    def player_turn(playernumber):
        while True:
            location = input(f"Player {playernumber}: ")
            if len(location) == 1:
                try:
                    location = int(location)
                    if location >= 1 <= 9 and instance.board[
                            location] is instance.option[0]:
                        player = instance.option[playernumber]
                        instance.place(player, location)
                        instance.print_board()
                        break
                    else:
                        invalid_input()
                except ValueError:
                    invalid_input()
            else:
                invalid_input()

    while True:
        for turn in range(1, 3):
            player_turn(turn)
            win_state = instance.win(turn)
            if win_state is not False:
                break
        if win_state is not False:
            break


if __name__ == "__main__":
    tictactoe = TicTacToe()
    print("Welcome to TicTacToe         | by davidschi")
    tictactoe.print_board()
    game(tictactoe)
